using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationStateController : MonoBehaviour
{
    Animator animator;
    int isWalkingHash;
    int isRunningHash;
    int isJumpingHash;
    int isFightingHash; 

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();

        isWalkingHash = Animator.StringToHash("isWalking");
        isRunningHash = Animator.StringToHash("isRunning");
        isJumpingHash = Animator.StringToHash("isJumping");
        isFightingHash = Animator.StringToHash("isFighting"); 
    }

    // Update is called once per frame
    void Update()
    {
        bool isRunning = animator.GetBool(isRunningHash); 
        bool isWalking = animator.GetBool(isWalkingHash);
        bool isJumping = animator.GetBool(isJumpingHash);
        bool isFighting = animator.GetBool(isFightingHash); 

        bool forwardPress = Input.GetKey("w");
        bool runPress = Input.GetKey("left shift");
        bool jumpPress = Input.GetKey("space");
        bool fightPress = Input.GetKey("e"); 

        if (!isWalking && forwardPress)
        {
            animator.SetBool(isWalkingHash, true); 
        }

        if (isWalking && !forwardPress)
        {
            animator.SetBool(isWalkingHash, false); 
        }

        if (!isRunning && (isWalking && runPress))
        {
            animator.SetBool(isRunningHash, true);
        }

        if (isRunning && (!isWalking || !runPress))
        {
            animator.SetBool(isRunningHash, false);
        }

        if (!isWalking && !isRunning && jumpPress)
        {
            animator.SetBool(isJumpingHash, true); 
        }

        if (isWalking && !runPress && jumpPress)
        {
            print("i am the king!");
            animator.SetBool(isWalkingHash, true); 
            animator.SetBool(isJumpingHash, true);
            animator.SetBool(isRunningHash, false); 
        }

        if (!jumpPress)
        {
            animator.SetBool(isJumpingHash, false); 
        }

        if (!isWalking && !runPress && !jumpPress && fightPress)
        {
            animator.SetBool(isFightingHash, true);
        }

        if (!fightPress)
        {
            animator.SetBool(isFightingHash, false);
        }
    }
}
