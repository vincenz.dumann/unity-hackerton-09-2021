using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Threading;
using System.Threading.Tasks;

public class playerMovementScript : MonoBehaviour
{
    public CharacterController controller;

    public float speed = 12f;
    public float gravity = -9.81f;
    public float jumpHeight = 1f; 

    public Transform groundCheck;
    public float groundDistance = 5f;
    public LayerMask groundMask; 

    Vector3 velocity;
    bool isGrounded; 

    // Update is called once per frame
    async Task Update()
    {
        bool runPress = Input.GetKey("left shift");
        isGrounded = Physics.CheckSphere(groundCheck.position, groundDistance, groundMask); 
        
        if (runPress)
        {
            speed = 20f;
        } else
        {
            speed = 12f; 
        }

        if (isGrounded && velocity.y < 0 )
        {
            velocity.y = 0f; 
        }
        
        float x = Input.GetAxis("Horizontal");
        float z = Input.GetAxis("Vertical");

        Vector3 move = transform.right * x + transform.forward * z;
        controller.Move(move * speed * Time.deltaTime);

        
        if (Input.GetKeyDown("space") && isGrounded)
        {
            isGrounded = false; 
            await Task.Delay(500);
            isGrounded = true; 
        }

        velocity.y += gravity * Time.deltaTime;
        controller.Move(velocity * Time.deltaTime); 
    }
}
